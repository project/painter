<?php


/**
 * Checks whether a chain name 
 *
 * @param unknown_type $chain_name
 * @param unknown_type $definitionHash
 * @param unknown_type $parameterHash
 * @param unknown_type $painterCache
 * @return unknown
 */
function _check_cache($chain_name, $definitionHash, $parameterHash, &$painterCache){
  
  $isCached = $painterCache[$chain_name];

  if ($painterCache[$chain_name]){
    // If the painter definition changed, remove all related files
    if ((int)$painterCache[$chain_name]['definition'] != (int)$definitionHash){
      $painterCache[$chain_name]['definition'] = $definitionHash;
      $files = '';
      if (count($painterCache[$chain_name]['variants'])>0){
        foreach($painterCache[$chain_name]['variants'] as $variantHash=>$variantFilename){
          file_delete($variantFilename);
          $files .= $variantFilename.' ';
          unset($painterCache[$chain_name]['variants'][$variantHash]);
        }
      }
      watchdog('info', $chain_name.' definition changed. Files removed:'."\n".$files);
      $isCached = FALSE;
    } else {
      // Painter definition not changed, does the target file exist?
      $isCached = file_exists($painterCache[$chain_name]['variants'][$parameterHash]);
    }
  } else {
    $painterCache[$chain_name] = array();
    $painterCache[$chain_name]['definition'] = $definitionHash;
    $painterCache[$chain_name]['variants'] = array();
  }
  
  return $isCached;
}

/**
 * Computes a hash for a nested array
 *
 * @param array
 * @return int computed hash
 */
function painter_hash_array($tohash){
  
  if (is_string($tohash)){
    $hashSum = 0;
    for ($a = 0; $a < strlen($tohash); $a++){
      $hashSum += ord(substr($tohash, $a, 1));
    }
    return (int) $hashSum;
  } 
  
  if (is_numeric($tohash)){
    return $tohash;
  } 
  
  if (is_array($tohash)) {
    $hashSum = 0;
    foreach($tohash as $hash){
      $hashSum += painter_hash_array($hash);
    }
    return (int) $hashSum;
  }
  return 0;
}



/**
 * Checks for and replaces a hexidecimal ([#]rrggbb | [#]rrggbbaa) color with its array representation
 * If $image is supplied, the color will be allocated to it, and returned
 * @param resource $image [optional] image to allocate color to
 * @param array/string $color_to_be
 * @return array representation of color: array($red, $green, $blue, $alpha)
 */
function painter_color($color_to_be, $image = NULL){
  
  // If it's a color already
  if (is_numeric($color_to_be)) {
    return $color;
  }
  
  // if it's already an array, don't bother converting
  if (is_array($color_to_be)){
    $rgba = $color_to_be;
  } else {
    
    $rgba = array(0, 0, 0, 0);
    if (is_string($color_to_be) && eregi('^\#?[a-f0-9]{6,8}$', $color_to_be)){
      // drop the #
      $color_to_be = substr($color_to_be, 1);
      
      if (strlen($color_to_be) == 6){
        sscanf($color_to_be, "%2x%2x%2x", $r, $g, $b);
        $a = 0;
      } else if (strlen($color_to_be) == 8) {
        sscanf($color_to_be, "%2x%2x%2x%2x", $r, $g, $b, $a);
        $a = $a > 127 ? 127 : $a;
      }
      $rgba = array($r, $g, $b, $a);
//      drupal_set_message('Converted '.$color_to_be.' to RGB('.$r.','.$g.','.$b.','.$a.')');
    }
  }
  
  if ($image) {
    return imagecolorresolvealpha($image, $rgba[0], $rgba[1], $rgba[2], $rgba[3]);
  }
  
  return $rgba;
}



/**
 * Replaces functions and [placeholders] within an argument with computed values and $variable['placeholder'] values,
 * and evaluates any contained expressions
 */
function painter_compute($argument, $variables){
  $substituted = painter_substitute($argument, $variables);
  return eval('return '.$substituted.';');
}

/**
 * Replaces functions and [placeholders] within an argument with computed values and $variable['placeholder'] values
 */
function painter_substitute($argument, $variables){
  if (is_string($argument)) {
    $processed = $argument;
    
    /* Substitute image heights */
    while ($start = eregi('width\([^\(]+\)', $processed, $search_result)){
      $variableKey = substr($search_result[0], 6, strlen($search_result) - 6);
      $processed = str_ireplace($search_result[0], (String) imagesx($variables[$variableKey]), $processed);
    }
    /* Substitute image widths */    
    while ($start = eregi('height\([^\(]+\)', $processed, $search_result)){
      $variableKey = substr($search_result[0], 7, strlen($search_result) - 6);
      $processed = str_ireplace($search_result[0], (String) imagesy($variables[$variableKey]), $processed);
    }
    /* Substitute variables */
    while ($start = ereg('\[[^\[]+\]', $processed, $search_result)){
      $variableKey = substr($search_result[0], 1, strpos($search_result[0], ']') - 1);
      
      //drupal_set_message('Substituting variable:['.$variableKey.'] with $variables['.$variableKey.'] ('.$variables[$variableKey].')');
      
      if (is_resource($variables[$variableKey])){
        return $variables[$variableKey];
      } else {
        $processed = str_ireplace($search_result[0], $variables[$variableKey], $processed);
      }
    }
    return $processed;
  }
  return $argument;
}



/**
 * Checks whether a theme provides Painter support (implements the theme_painters hook)
 * OR checks whether a chain with specified name is supported
 * @param string $chain name [optional] check if a specific chain is supported
 * @return boolean
 */
function painter_theme_supports($chain_name = NULL){
  global $theme_key;
  $function = $theme_key.'_painters';
  if (!function_exists($function)){
    return FALSE;
  }

  if ($chain_name){ // check if a chain is supported
    $cached = $function(NULL, TRUE);
    $nonCached = $function(NULL, FALSE);
    $supported = ($cached[$chain_name] | $nonCached[$chain_name]);
    return $supported;
  }
  return TRUE;
}



/**
 * Resolves whether a resulting image should be returned or stored (and stores it if needs be)
 */
function painter_resolve_destination($result, $args, &$variables, $resulting = 'as'){
  // If the resulting image should be stored as a different image
  if ($args[$resulting]){
    if (is_resource($variables[$args[$resulting]])){
      imagedestroy($variables[$args[$resulting]]);
    }
    $variables[$args[$resulting]] = $result;
    return;
  }
  // If a variable key was provided as source, replace the source with the resulting image
  if ($args['image'] && is_string($args['image'])) {
    $key = substr($args['image'], 1, strlen($args['image']) - 2);
    if (is_resource($variables[$key])){
      imagedestroy($variables[$key]);
    }
    $variables[$key] = $result;
    return;
  }
  // If the canvas is to be manipulated
  if ($source = $variables['canvas']) {
    if (is_resource($variables[$args['canvas']])){
      imagedestroy($variables[$args['canvas']]);
    }
    $variables['canvas'] = $result;
    return;
  }
  return $result;
}