<?php


/**
 * @file
 * A module to enable image creation and manipulation on the fly
 */

// Utility functions
include_once('utils.php');



// ---------------------------------------------------------------------------- Hooks



/**
 * Implementation of hook_perm()
 */
function painter_menu_perm() {
	return array('administer painter');
}



/**
 * Implementation of hook_menu()
 * @param boolean $may_cache
 */
function painter_menu($may_cache){
	$items = array ();

	if (!$may_cache){
		$items[] = array(
      'path' => 'admin/settings/dynamic-images',
      'title' => t('Dynamic images'),
      'description' => t('Manage dynamic images'),
      'callback' => 'painter_view_capabilities',
      'access' => user_access('administer painter'),
		);

    $items[] = array(
      'path' => 'admin/settings/dynamic-images/capabilities',
      'title' => t('Capabilities'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );		
		
    $items[] = array(
      'path' => 'admin/settings/dynamic-images/render-cache',
      'title' => t('Render cache'),
      'callback' => 'painter_view_render_cache',
      'access' => user_access('administer painter'),
      'type' => MENU_LOCAL_TASK,
    );
    
    // Callbacks
    $items[] = array(
      'path' => 'admin/settings/dynamic-images/clear-cache',
      'access' => user_access('administer painter'),
      'callback' => 'painter_clear_cache',
      'type' => MENU_CALLBACK,
    );
  }
	
	return $items;
}

/**
 * Implementation of hook_provides_operations
 *
 * @return unknown
 */
function painter_provides_operations(){
  return array('Load image', 'Save image', 'Move variable', 'Log variable');
}



// ---------------------------------------------------------------------------- Views



/**
 * Information form
 */
function painter_view_capabilities(){
  $header = array(t('Operations'), t('Provided by'));
  $rows = array();
  
  $plugins = module_implements('provides_operations');
  
  foreach($plugins as $plugin){
    $ops = module_invoke($plugin, 'provides_operations');
    $rows[] = array(
      'operations' => implode(', ', $ops), 
      'name' => $plugin, 
    );
  }
  if (empty($rows)){
    $rows[] = array(
      'row' => array(array('data' => t('No operations defined.'), 'colspan' => '2')),
    );
  }
  
  return theme('table', $header, $rows, array());
}

/**
 * Cache view
 */
function painter_view_render_cache(){
  return drupal_get_form('painter_form_cache');
}



// ---------------------------------------------------------------------------- Forms



function painter_form_cache(){
  $cache = variable_get('painter_cache', array());
  $header = array(t('Painter name'), t('Variant count'), t('Files'));
  $rows = array();
    
  if (count($cache) > 0){
    foreach($cache as $chain_name=>$chain_info){
      $rows[] = array(
        'name' => $chain_name,
        'variant_count' => count($chain_info['variants']),
        implode(', ', $chain_info['variants']),
      );
    }
  } else {
    $rows[] = array(array('data' => t('Cache is empty.'), 'colspan' => 3));
  }
  
  $form = array();

  $form['info'] = array( 
    '#value' => theme('table', $header, $rows, array('id' => 'painter')),
  );
  
  $form['submit'] = array( 
    '#type' => 'submit', 
    '#value' => t('Clear cache'), 
  );

  return $form;
}

function painter_form_cache_submit(){
  variable_del('painter_cache');
  if ($handle = opendir('files/dynamic')){
    while (false !== ($file = readdir($handle))) {
      file_delete('files/dynamic/'.$file);
    }
  }
  return 'admin/settings/dynamic-images/render-cache';
}



// ---------------------------------------------------------------------------- Painting



/**
 * Returns the path to the resulting image of a painter rendering. If the image does not exist, it is first rendered and saved. 
 * 
 * @param string $chain_name
 * @param array/variable $params optional paramer(s) single parameters may be passed directly, multiple parameters should be wrapped in an array
 * @param string $extension optional format to output ['.png' | '.gif' | '.jpg'], defaults to '.png'
 */
function painter_get_image($chain_name, $params = NULL, $extension = '.png'){
  
  global $theme_key;
  
  if ($params && !is_array($params)) $params = array('default' => $params);

  // Get painter to execute
  $themeHook = $theme_key.'_painters';
  if (!function_exists($themeHook)) {
    return FALSE;
  }

  // For definition hashes (without parameters)
  $cachedPainterDefs = $themeHook(array(), TRUE);
  $nonCachedPainterDefs = $themeHook(array(), FALSE);
  $painterDefinition = $nonCachedPainterDefs[$chain_name] ? $nonCachedPainterDefs[$chain_name] : $cachedPainterDefs[$chain_name]; 
  
  // Painters with parameters 
  $cachedPainters = $themeHook($params, TRUE);
  $nonCachedPainters = $themeHook($params, FALSE);
  $painter = $nonCachedPainters[$chain_name] ? $nonCachedPainters[$chain_name] : $cachedPainters[$chain_name]; 
  
  // Check cache
  $painterCache = variable_get('painter_cache', array());
  $definitionHash = painter_hash_array($painterDefinition);
  $parameterHash = painter_hash_array($params);
  $isCached = _check_cache($chain_name, $definitionHash, $parameterHash, &$painterCache);
  
  $filename = $params['#output'] 
    ? $params['#output']
    : file_directory_path().'/dynamic/'.$theme_key.'_'.$chain_name.'_'.$definitionHash.'_'.$parameterHash.$extension;
  
  if ($nonCachedPainters[$chain_name] || !$isCached){ // Do rendering if non-cached or not cached yet

    watchdog('info', 'Rendering ['.$chain_name.'] with parameters: '.implode(',', $params), WATCHDOG_NOTICE);
    
    // execute
    $variables = array('#output' => $filename);
    $result = painter_execute($chain_name, $painter, &$variables);
    $filename = $variables['#output'];
    
    if (!$result){
      watchdog('Rendering error', 'Rendering failed! Aborting output ['.$filename.']', WATCHDOG_ERROR);
      return;
    }
    
    // If canvas exists, output to file
    if ($variables['canvas']){
      painter_invoke('save image', array(
          'filename' => $filename,
        ), &$variables
      );
    }

    // Clean up after the painter
    foreach($variables as $label=>$variable) {
      if (is_resource($variable)) {
        imagedestroy($variable);
      }
    }
    
    // Cache filename, if needed
    if ($cachedPainters[$chain_name]) {
      watchdog('info', 'Caching render of ['.$chain_name.'] with parameters: '.implode(',', $params).' into cache', WATCHDOG_NOTICE);
      $painterCache[$chain_name]['variants'][$parameterHash] = $filename;
    }
    
    variable_set('painter_cache', $painterCache);
    return '/'.$filename;
  } else { // Return the filename we've got cached
    variable_set('painter_cache', $painterCache);
    return '/'.$painterCache[$chain_name]['variants'][$parameterHash];
  }
}



/**
 * Invokes a plugin operation
 * @param $module name of the plugin module that provides the operation [optional]
 * @param $arguments to pass to the operation
 * @param array $variables containing painter variables [optional]
 * 
 * @return array $variables containing supplied variables, if any, and the results of the operation
 */
function painter_invoke($operation, $arguments, &$variables = NULL, $chain_name = NULL){

  $module = $arguments['module'];

  $hook = 'op_'.str_replace(' ', '_', $operation);

  if(!is_string($operation)){
    watchdog('Rendering error', 'Parameters without operatior in '. ($chain_name == NULL ? 'anonymous invocation' : ' painter '.$chain_name.'!'), WATCHDOG_ERROR);
    return FALSE;
  }
  
  // Find modules that implement the operation
  if ($module == NULL){
    $modules = module_implements($hook);
    $module = $modules[0];
    $foundCount = count($modules);
  
    if ($foundCount == 0) {
      watchdog('Rendering error', 'No module found that implements the \''.$operation.'\' operation in '. ($chain_name == NULL ? 'anonymous invocation' : ' painter '.$chain_name) .'!', WATCHDOG_ERROR);
      return FALSE;
    }
        
    if ($foundCount > 1){
      watchdog('warning', 'Multiple modules implement the \''.$operation.'\' operation! Operartion in'.$module.' selected.', WATCHDOG_ERROR);
    }
  }
  
  $function = $module.'_'.$hook;
  return $function($arguments, &$variables);
}



/**
 * Excecutes a chain of plugin operations, and returns the resulting processed variables
 * @param array $painter
 * @param variables;
 */
function painter_execute($chain_name, $painter, &$variables = array()){
  
  watchdog('Notice', 'Executing '.$chain_name, WATCHDOG_NOTICE);
    
  $index = 0;
  foreach ($painter as $label=>$args){
    $returned = painter_invoke($args['operation'], $args, &$variables, $chain_name);
    //print '<pre> '.$label.': '; print_r($variables); print '</pre>';
    
    if (!is_null($returned) && !$returned){ // Function explicitly returned FALSE - operation failed
      watchdog('Rendering error', ($label ? $label.':' : '').'The "'.$args['operation'].'" operation failed in painter chain "'.$chain_name.'"!', WATCHDOG_ERROR);
      return FALSE;
    }
  }

  return $variables;
}



// ---------------------------------------------------------------------------- Built in ops



function painter_op_load_image($args, &$variables){
  
  $filename = ltrim($args['filename'], '/'); 
  
  $ext = strtolower(substr($filename, strlen($filename) -4));
  
  if (!file_exists($filename)){
    watchdog('Rendering error', 'Could not load "'.$args['filename'].'" File not found!', WATCHDOG_ERROR);
    return FALSE;
  }
  
  switch ($ext){
    case '.gif': $image = imagecreatefromgif($filename); break;
    case '.jpg': $image = imagecreatefromjpg($filename); break;
    case '.png': $image = imagecreatefrompng($filename); break;
  }
  
  if ($args['as']) {
    $variables[$args['as']] = $image;
  } else {
    $variables['canvas'] = $image;
  }
}



function painter_op_save_image($args, &$variables = NULL){
  
  $raw = painter_substitute($args['filename'], $variables);
  $filename = substr(strrchr($raw, '/'), 1);
  $path = substr($raw, 0, strlen($raw) - strlen($filename));
  
  $full_name = realpath($path).'/'.$filename;
  
  watchdog('info', 'Writing image :'.$full_name, WATCHDOG_NOTICE);
  
  $ext = strtolower(substr($filename, strlen($filename) - 4));
  
  if ($args['image']) {
    $image = painter_substitute($args['image'], $variables);
  } else {
    $image = $variables['canvas'];
  }
  if (!$image) {
    watchdog('Rendering error', 'No image to output! '.($args['image'] ? 'Check "image" parameter!' : 'No canvas has been created, and \'image\' parameter not supplied!'), WATCHDOG_ERROR);
    return FALSE;
  }
  
  switch ($ext){
    case '.gif': imagegif($image, $full_name); break;
    case '.jpg': imagejpg($image, $full_name); break;
    case '.png': imagepng($image, $full_name); break;
  }
}



function painter_op_log_variable($args, &$variables = NULL){
  watchdog('Painter output', 'variable[\''.$args['variable'].'\'] = '.$variables[$args['variable']], WATCHDOG_NOTICE);
}



function painter_op_move_variable($args, &$variables = NULL){
//  print 'Moving '.$args['from']. ' to '.$args['to'];
  $variables[$args['to']] = $variables[$args['from']];
  unset($variables[$args['from']]);
}